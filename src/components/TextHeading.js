import React from 'react';
import { Text, View } from 'react-native';

const TextHeading = (props) => {
    const { textHeadingStyle } = styles;
    return (
        <Text style={textHeadingStyle}>
            {props.textHeading}
        </Text>
    );
}

const styles = {
    textHeadingStyle : {
        fontSize: 32,
        color: '#151F2B',
        fontWeight: '500',
        marginBottom: 20,
    }
}

export default TextHeading;
