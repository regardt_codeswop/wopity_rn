import React, { Component } from 'react';
import { View, Text, Image, ScrollView, Button, Linking } from 'react-native';
// import { Button } from 'react-native-elements';
import TextHeading from '../../src/components/TextHeading';

class Welcome extends Component  {

    state = {
        introText : [
            { blurp: 'Only the best spots in Cape Town'},
            { blurp: 'Request Uber Rides directly to any of the spots on our app'},
            { blurp: 'Find all the latest events in Cape Town'}
        ]
    };

    renderIntroText() {
        const { introText, listItemStyle, scrollStyle } = styles;
        return (
            <View>
                { this.state.introText.map((listItem) =>
                    <View key={listItem.blurp} style={listItemStyle}>
                    <Text style={introText} key={listItem.blurp}>
                        {listItem.blurp}
                    </Text>
                    </View>
                )}
            </View>
        );
    }
    render() {
        const { welcomeStyle, logoStyle, buttonStyle, welcomeStyleButton} = styles;

        return (
            <View style={welcomeStyle}>
                <Image style={logoStyle} source={require('../../public/graphics/logo.png')} />
                <TextHeading textHeading="Don't be boring in Cape Town."/>
                <View>{this.renderIntroText()}</View>
                <View style={welcomeStyleButton}>
                    <Button
                      onPress={() => Linking.openURL('http://google.co.za')}
                      style={buttonStyle}
                      title="Get Started"
                      color="#5ac7da"
                      accessibilityLabel="Learn more about this purple button"
                    />
                    <Button
                      onPress={() => Linking.openURL('http://google.co.za')}
                      style={buttonStyle}
                      title="Log in"
                      color="#DA1C5C"
                      accessibilityLabel="Learn more about this purple button"
                    />
                </View>
            </View>
        );
    }
};

const styles = {
    welcomeStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 30,
        paddingLeft: 25,
        paddingRight: 25
    },
    welcomeStyleButton: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        marginTop: 30,
        paddingLeft: 25,
        paddingRight: 25
    },
    introText: {
        color: '#606163',
        fontSize: 20,
        marginTop: 10,
        marginBottom: 10,
    },
    logoStyle: {
        marginBottom: 10
    },
    listItemStyle: {
        borderBottomColor: '#ddd',
        borderBottomWidth: 1,
    },
    scrollStyle: {
        flex:1
    },
    buttonStyle: {
        padding: 50
    }
};

export default Welcome;
