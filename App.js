import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';
import Header from './src/components/Header';
import Welcome from './src/views/Welcome';


const instructions = Platform.select({
  ios: 'iOS',
  android: 'Android'
});

const App = () => (
        <View>
            <Header headerText={'Welcome'}/>
            <Welcome />
        </View>
);

const styles = {
    welcomeStyle: {
        flex: 1
    }
}

export default App;
